-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table sciencereport.tbl_department
CREATE TABLE IF NOT EXISTS `tbl_department` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` smallint(6) DEFAULT NULL COMMENT 'це поле буде зберігати "батька" цього пункту. Таким чином ми позбудемося двох окремих таблиць - "відділи" та "факультети". У одній таблиці і фаультети і відділи і лабораторіі - по суті організаційна деревовидна структура університету. У кафедр parent_id повинен вказувати на id факультету (а по суті - на відділ з флагом faculty-flag=1). Справа у тому що у структурі будуть і центри, і лабораторії і відділи т.д. Таку табличку я в принципі тобі можу дати - бо на сайті УжНУ всі категорії факультетів та кафедр так сами деревовидно побудовані.',
  `faculty_flag` tinyint(1) DEFAULT '0' COMMENT 'Флаг факультета: 0 - не факультет, 1 - факультет',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_department: ~0 rows (approximately)
DELETE FROM `tbl_department`;
/*!40000 ALTER TABLE `tbl_department` DISABLE KEYS */;
INSERT INTO `tbl_department` (`id`, `alias`, `title`, `parent_id`, `faculty_flag`) VALUES
	(1, 'faculty', 'Факультети', 0, 0),
	(2, 'deps', 'Науково-дослідні інститути, центри та лабораторії', 0, 0),
	(3, 'fhistory', 'Історичний факультет', 1, 0),
	(4, 'math', 'Математичний факультет', 1, 0),
	(5, 'susp_centre', 'ННЦ суспільно-політичних досліджень', 2, 0),
	(6, 'uahistory', 'Кафедра історії України', 3, 0),
	(7, 'aaaa', 'aaaaaa', 3, 0);
/*!40000 ALTER TABLE `tbl_department` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_indicator
CREATE TABLE IF NOT EXISTS `tbl_indicator` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UNSIGNED - 0..255 - більше 255 не буде (за замовчуванням - SIGNED -127...127)\\n',
  `position` tinyint(4) unsigned DEFAULT NULL COMMENT 'для сортування у формі (порядковий номер У РОЗДІЛІ (SECTION)',
  `section` tinyint(4) DEFAULT NULL COMMENT 'РОЗДІЛ (звичайний порядковий номер). Їх буде 6-9 розділів - просто зберігати їх назви у масиві додатку - щоб витягувати їх назви на форму.  Створювати окрему таблицю для 6 записів нема сенсу.',
  `title` varchar(255) DEFAULT NULL,
  `hint` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_indicator: ~3 rows (approximately)
DELETE FROM `tbl_indicator`;
/*!40000 ALTER TABLE `tbl_indicator` DISABLE KEYS */;
INSERT INTO `tbl_indicator` (`id`, `position`, `section`, `title`, `hint`) VALUES
	(1, 5, 1, 'Видання підручника для ВНЗ за рекомендацією Вченої ради УжНУ (у календарному році, що завершився)', NULL),
	(2, 1, 1, 'Видання навчального посібника для ВНЗ за рекомендацією Вченої ради УжНУ (у календарному році, що завершився)', NULL),
	(3, 0, 2, 'Видання підручника для загальноосвітньої школи за рекомендацією Вченої ради УжНУ   (у календарному році, що завершився)', NULL);
/*!40000 ALTER TABLE `tbl_indicator` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_person
CREATE TABLE IF NOT EXISTS `tbl_person` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) DEFAULT NULL COMMENT 'Імя',
  `lastname` varchar(50) DEFAULT NULL COMMENT 'Прізвище',
  `secondname` varchar(30) DEFAULT NULL COMMENT 'По-батькові',
  `department_id` smallint(6) DEFAULT NULL COMMENT 'відділ (кафедра), у якому працює людина',
  `user_id` smallint(6) DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL COMMENT 'дата останнього створення\\редагування',
  `status` enum('empty','inprogress','closed') DEFAULT 'empty' COMMENT 'Статус: пусто \\ у процессі заповнення \\ заповнена і закрита для редагування',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_person_tbl_user1_idx` (`user_id`),
  KEY `fk_tbl_person_tbl_department1_idx` (`department_id`),
  CONSTRAINT `fk_tbl_person_tbl_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_person_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_person: ~4 rows (approximately)
DELETE FROM `tbl_person`;
/*!40000 ALTER TABLE `tbl_person` DISABLE KEYS */;
INSERT INTO `tbl_person` (`id`, `firstname`, `lastname`, `secondname`, `department_id`, `user_id`, `date_edit`, `status`) VALUES
	(2, 'Іван', 'Іванович', 'Іванов', NULL, 12, NULL, 'inprogress'),
	(3, 'Петро', 'Петрович', 'Петров', NULL, 12, NULL, 'inprogress');
/*!40000 ALTER TABLE `tbl_person` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_person_indicator
CREATE TABLE IF NOT EXISTS `tbl_person_indicator` (
  `person_id` smallint(6) NOT NULL,
  `indicator_id` tinyint(4) unsigned NOT NULL,
  `total` float DEFAULT NULL COMMENT 'FLOAT бо будуть не цілі',
  `notes` text COMMENT '"Примітки" з паперової форми - MEMO-поле яке за потребою можна заповнити на формі до кожного з показника для пояснення на основі чого було заповнено цей показник.',
  KEY `fk_tbl_person_indicator_tbl_person1_idx` (`person_id`),
  KEY `fk_tbl_person_indicator_tbl_indicator_idx` (`indicator_id`),
  CONSTRAINT `fk_tbl_person_indicator_tbl_indicator` FOREIGN KEY (`indicator_id`) REFERENCES `tbl_indicator` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_person_indicator_tbl_person1` FOREIGN KEY (`person_id`) REFERENCES `tbl_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_person_indicator: ~6 rows (approximately)
DELETE FROM `tbl_person_indicator`;
/*!40000 ALTER TABLE `tbl_person_indicator` DISABLE KEYS */;
INSERT INTO `tbl_person_indicator` (`person_id`, `indicator_id`, `total`, `notes`) VALUES
	(3, 3, 2342340, 'dfsfsafd');
/*!40000 ALTER TABLE `tbl_person_indicator` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) DEFAULT NULL COMMENT 'Не повинен бути имейлом - бо у багатьох вже є логіни з сайту університету - це чатсина перед @uzhnu.edu.ua їх офіційної пошти кафедри',
  `password` varchar(255) DEFAULT NULL COMMENT 'тут треба хеш пасворда тримати',
  `email` varchar(50) DEFAULT NULL,
  `preset_dep_id` smallint(6) DEFAULT NULL COMMENT 'Це налаштування для користувача - який відділ\\кафедру він буде заповнювати - але на формі в принципі може вибрати будь який з іншого відділу.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_tbl_user_tbl_department1_idx` (`preset_dep_id`),
  CONSTRAINT `fk_tbl_user_tbl_department1` FOREIGN KEY (`preset_dep_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_user: ~5 rows (approximately)
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id`, `login`, `password`, `email`, `preset_dep_id`) VALUES
	(12, 'new.user', '6dbd0fe19c9a301c4708287780df41a2', 'newuser@gmail.com', NULL),
	(14, 'ffdgfdhfdh', '42df55041d8d6de9e5e80a2c055590a3', 'fd@com.com', NULL),
	(15, 'sadfasdf', '6dbd0fe19c9a301c4708287780df41a2', 'new.user@com.ua', NULL),
	(16, 'Lol', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'lol@fm.com', NULL),
	(17, 'sadf_dsdsd.sdfds', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'sadf@kom.com', NULL),
	(19, 'Lololo', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'john@doeee.com', 6);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
