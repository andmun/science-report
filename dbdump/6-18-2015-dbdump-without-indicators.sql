-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table sciencereport.tbl_department
CREATE TABLE IF NOT EXISTS `tbl_department` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` smallint(6) DEFAULT NULL COMMENT 'це поле буде зберігати "батька" цього пункту. Таким чином ми позбудемося двох окремих таблиць - "відділи" та "факультети". У одній таблиці і фаультети і відділи і лабораторіі - по суті організаційна деревовидна структура університету. У кафедр parent_id повинен вказувати на id факультету (а по суті - на відділ з флагом faculty-flag=1). Справа у тому що у структурі будуть і центри, і лабораторії і відділи т.д. Таку табличку я в принципі тобі можу дати - бо на сайті УжНУ всі категорії факультетів та кафедр так сами деревовидно побудовані.',
  `faculty_flag` tinyint(1) DEFAULT '0' COMMENT 'Флаг факультета: 0 - не факультет, 1 - факультет',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_department: ~191 rows (approximately)
DELETE FROM `tbl_department`;
/*!40000 ALTER TABLE `tbl_department` DISABLE KEYS */;
INSERT INTO `tbl_department` (`id`, `alias`, `title`, `parent_id`, `faculty_flag`) VALUES
	(10, 'biolog', 'Біологічний факультет', 38, 0),
	(11, 'geograph', 'Географічний факультет', 38, 0),
	(12, 'humanitar', 'Гуманітарно-природничий факультет з угорською мовою навчання', 38, 0),
	(14, 'health', 'Факультет здоров\'я людини', 38, 0),
	(15, 'engineer', 'Інженерно-технічний факультет', 38, 0),
	(16, 'fit', 'Факультет інформаційних технологій', 38, 0),
	(17, 'fhistory', 'Історичний факультет', 38, 0),
	(18, 'math', 'Математичний факультет', 38, 0),
	(19, 'medical', 'Медичний факультет', 38, 0),
	(20, 'zoology', 'Кафедра зоології', 10, 0),
	(21, 'botanic', 'Кафедра ботаніки', 10, 0),
	(22, 'entomol', 'Кафедра ентомології та збереження біорізноманіття', 10, 0),
	(23, 'genetics', 'Кафедра генетики, фізіології рослин та мікробіології', 10, 0),
	(24, 'plodi', 'Кафедра плодоовочівництва і виноградарства', 10, 0),
	(25, 'zemlevp', 'Кафедра землевпорядкування та кадастру', 11, 0),
	(26, 'fiz_geo', 'Кафедра фізичної географії та раціонального природокористування', 11, 0),
	(27, 'forests', 'Кафедра лісівництва', 11, 0),
	(38, 'faculty', 'Факультети', 0, 0),
	(70, 'library', 'Бібліотечно-інформаційний центр', 89, 0),
	(89, 'deps', 'Науково-дослідні інститути, центри та лабораторії', 0, 0),
	(91, 'susp_centre', 'ННЦ суспільно-політичних досліджень', 89, 0),
	(99, 'ndi_physics', 'НДІ фізики і хімії твердого тіла', 89, 0),
	(100, 'ndi_carpath', 'НДІ карпатознавства', 89, 0),
	(102, 'ndi_regions', 'НДІ політичної регіоналістики', 89, 0),
	(103, 'ndi_ukraine', 'НДІ україністики імені М. Мольнара', 89, 0),
	(106, 'ndi_analytics', 'НДІ засобів аналітичної техніки', 89, 0),
	(108, 'ndi_government', 'Інститут державного управління та регіонального розвитку', 89, 0),
	(109, 'ndi_ecology', 'Інститут еколого-релігійних студій', 89, 0),
	(110, 'itc', 'Центр інформаційних технологій', 89, 0),
	(111, 'nni_euroint', 'ННІ євроінтеграційних досліджень', 89, 0),
	(112, 'nni_linguistics', 'ННІ лінгвістики і міжкультурної комунікації', 89, 0),
	(113, 'centre_hungary', 'Центр гунгарології', 89, 0),
	(114, 'centre_slovak', 'Центр історичних студій з богемістики і словакістики', 89, 0),
	(116, 'centre_business', 'Бізнес-центр', 89, 0),
	(117, 'centre_psy', 'Центр психологічної служби', 89, 0),
	(119, 'centre_drugs', 'Центр з випробування нових лікарських засобів', 89, 0),
	(120, 'lab_space', 'Лабораторія фізичної електроніки з лабораторією космічних досліджень', 89, 0),
	(122, 'lab_eco', 'Лабораторія охорони природних екосистем', 89, 0),
	(124, 'dep_irelations', 'Відділ міжнародного освітнього та наукового співробітництва', 89, 0),
	(126, 'ndc_innov', 'Науково-дослідний центр інновацій та розвитку', 89, 0),
	(130, 'pdodp', 'Факультет післядипломної освіти та доуніверситетської підготовки', 38, 0),
	(132, 'iif', 'Факультет іноземної філології', 38, 0),
	(133, 'lnni', 'Львівський навчально-науковий центр', 89, 0),
	(134, 'fstomat', 'Стоматологічний факультет', 38, 0),
	(135, 'fsusp', 'Факультет суспільних наук', 38, 0),
	(136, 'ftourism', 'Факультет туризму та міжнародних комунікацій', 38, 0),
	(137, 'fphysics', 'Фізичний факультет', 38, 0),
	(138, 'ffilology', 'Філологічний факультет', 38, 0),
	(139, 'fchemistry', 'Хімічний факультет', 38, 0),
	(140, 'flaw', 'Юридичний факультет', 38, 0),
	(141, 'dep_ksport', 'Кафедра фізичного виховання', 219, 0),
	(143, 'hu_history', 'Кафедра історії Угорщини та європейської інтеграції', 12, 0),
	(144, 'hu_filology', 'Кафедра угорської філології', 12, 0),
	(145, 'hu_fizmath', 'Кафедра фізико-математичних дисциплін', 12, 0),
	(146, 'fiz_rise', 'Кафедра теорії і методики фізичного виховання', 14, 0),
	(147, 'rehabilitation', 'Кафедра фізичної реабілітації', 14, 0),
	(148, 'kaf_health', 'Кафедра основ здоров’я', 14, 0),
	(149, 'public_law', 'Кафедра адміністративного, фінансового, інформаційного та європейського публічного права', 140, 0),
	(150, 'm_law', 'Кафедра міжнародного приватного права, правосуддя та адвокатури', 140, 0),
	(151, 'm_krim_law', 'Кафедра кримінально-правових дисциплін та міжнародного кримінального права', 140, 0),
	(152, 'inter_law', 'Кафедра міжнародного права', 140, 0),
	(154, 'network_systems', 'Кафедра комп’ютерних систем і мереж', 15, 0),
	(155, 'manuf_engneer', 'Кафедра технології машинобудування', 15, 0),
	(156, 'instrumentation', 'Кафедра приладобудування', 15, 0),
	(157, 'city_building', 'Кафедра міського будівництва і господарства', 15, 0),
	(158, 'e_systems', 'Кафедра електронних систем', 15, 0),
	(159, 'software', 'Кафедра програмного забезпечення систем', 16, 0),
	(160, 'it_technology', 'Кафедра інформаційних управляючих систем та технологій', 16, 0),
	(161, 'i_fiz_math', 'Кафедра інформатики та фізико-математичних дисциплін', 16, 0),
	(162, 'uahistory', 'Кафедра історії України', 17, 0),
	(163, 'oldworld', 'Кафедра історії Стародавнього світу і Середніх віків', 17, 0),
	(164, 'newworld', 'Кафедра нової і новітньої історії та історіографії', 17, 0),
	(165, 'algebra', 'Кафедра алгебри', 18, 0),
	(166, 'matanalis', 'Кафедра теорії ймовірностей і математичного аналізу', 18, 0),
	(167, 'difmat', 'Кафедра диференціальних рівнянь та математичної фізики', 18, 0),
	(168, 'itmath', 'Кафедра кібернетики і прикладної математики', 18, 0),
	(169, 'sysanalis', 'Кафедра cистемного аналізу та теорії оптимізації', 18, 0),
	(170, 'medvnyt', 'Кафедра пропедевтики внутрішніх хвороб', 19, 0),
	(171, 'medgigiena', 'Кафедра соціальної медицини та гігієни', 19, 0),
	(172, 'medbiochemistry', 'Кафедра біохімії, фармакології та фізичних методів лікування', 19, 0),
	(173, 'medtherapy', 'Кафедра факультетської терапії', 19, 0),
	(174, 'medsurgery', 'Кафедра хірургічних хвороб', 19, 0),
	(175, 'medkid', 'Кафедра дитячих хвороб', 19, 0),
	(177, 'medhospital', 'Кафедра госпітальної терапії', 19, 0),
	(178, 'surgery', 'Кафедра загальної хірургії', 19, 0),
	(179, 'medfisio', 'Кафедра фізіології та патофізіології', 19, 0),
	(180, 'mednerv', 'Кафедра неврології, нейрохірургії та психіатрії', 19, 0),
	(181, 'medgynecology', 'Кафедра акушерства та гінекології', 19, 0),
	(182, 'medonko', 'Кафедра онкології', 19, 0),
	(183, 'medvenus', 'Кафедра шкірних та венеричних хвороб', 19, 0),
	(184, 'medanatomy', 'Кафедра анатомії людини та гістології ', 19, 0),
	(185, 'stotherapy', 'Кафедра терапевтичної стоматології', 134, 0),
	(186, 'stoface', 'Кафедра хірургічної стоматології щелепно-лицевої хірургії та онкостоматології', 134, 0),
	(187, 'stokid', 'Кафедра дитячої стоматології', 134, 0),
	(188, 'stoortoped', 'Кафедра ортопедичної стоматології', 134, 0),
	(189, 'sysphilo', 'Кафедра філософії', 135, 0),
	(190, 'syspsycho', 'Кафедра психології', 135, 0),
	(191, 'syspolit', 'Кафедра політології і державного управління', 135, 0),
	(192, 'syssocio', 'Кафедра соціології і соціальної роботи', 135, 0),
	(193, 'comtur', 'Кафедра туризму', 136, 0),
	(194, 'turinfra', 'Кафедра туристичної інфраструктури та сервісу', 136, 0),
	(195, 'turcom', 'Кафедра міжкультурної комунікації', 136, 0),
	(197, 'fiztheory', 'Кафедра теоретичної фізики', 137, 0),
	(198, 'fizsemicon', 'Кафедра фізики напівпровідників', 137, 0),
	(199, 'fizkvant', 'Кафедра квантової електроніки', 137, 0),
	(200, 'fiztverd', 'Кафедра твердотільної електроніки з/с Інформаційної безпеки', 137, 0),
	(201, 'fizopt', 'Кафедра оптики', 137, 0),
	(202, 'filua', 'Кафедра української мови', 138, 0),
	(203, 'filsl', 'Кафедра словацької філології', 138, 0),
	(204, 'filshur', 'Кафедра журналістики', 138, 0),
	(205, 'filru', 'Кафедра російської мови', 138, 0),
	(206, 'filualit', 'Кафедра української літератури', 138, 0),
	(207, 'filrulit', 'Кафедра російської літератури', 138, 0),
	(208, 'ximorg', 'Кафедра органічної хімії', 139, 0),
	(209, 'ximneorg', 'Кафедра неорганічної хімії', 139, 0),
	(210, 'ximeco', 'Кафедра екології та охорони навколишнього середовища', 139, 0),
	(211, 'ximfiz', 'Кафедра фізичної та колоїдної хімії', 139, 0),
	(212, 'ximalalytics', 'Кафедра аналітичної хімії', 139, 0),
	(213, 'urcivil', 'Кафедра цивільного права', 140, 0),
	(214, 'urconts', 'Кафедра конституційного права та порівняльного правознавства', 140, 0),
	(215, 'urgosp', 'Кафедра господарського права', 140, 0),
	(216, 'urcrim', 'Кафедра кримінального права', 140, 0),
	(217, 'urtheory', 'Кафедра теорії та історії держави і права', 140, 0),
	(218, 'zagpsycho', 'Кафедра педагогіки та психології', 219, 0),
	(219, 'zagalkaf', 'Загальноуніверситетські кафедри', 0, 0),
	(220, 'posttherapy', 'Кафедра терапії та сімейної медицини', 130, 0),
	(221, 'postradionko', 'Кафедра радіології та онкології', 130, 0),
	(222, 'postpulm', 'Кафедра пульмонології, фтизіатрії та фізіотерапії', 130, 0),
	(223, 'postsurgery', 'Кафедра хірургічних дисциплін', 130, 0),
	(224, 'postcommon', 'Кафедра громадського здоров’я', 130, 0),
	(227, 'postmother', 'Кафедра охорони материнства та дитинства', 130, 0),
	(228, 'interrelations', 'Факультет міжнародних відносин', 38, 0),
	(229, 'kafkraino', 'Кафедра країнознавства', 233, 0),
	(230, 'kafinterecon', 'Кафедра міжнародних економічних відносин', 228, 0),
	(231, 'kafinterrel', 'Кафедра міжнародних відносин', 228, 0),
	(232, 'kaftranslate', 'Кафедра теорії та практики перекладу', 228, 0),
	(233, 'interbusiness', 'Факультет міжнародної політики, менеджменту та бізнесу', 38, 0),
	(234, 'economics', 'Економічний факультет', 38, 0),
	(235, 'econcompany', 'Кафедра економіки підприємства', 234, 0),
	(236, 'econfin', 'Кафедра фінансів і банківської справи', 234, 0),
	(237, 'econoblic', 'Кафедра обліку і аудиту', 234, 0),
	(240, 'interpolit', 'Кафедра економічної теорії', 234, 0),
	(241, 'intermanage', 'Кафедра міжнародного бізнесу, логістики та менеджменту', 233, 0),
	(242, 'interling', 'Кафедра прикладної лінгвістики', 233, 0),
	(243, 'kafger', 'Кафедра німецької філології', 132, 0),
	(244, 'kafrom', 'Кафедра класичної та румунської філології', 132, 0),
	(245, 'kaffra', 'Кафедра французької мови та зарубіжної літератури', 132, 0),
	(246, 'kafeng', 'Кафедра англійської філології', 132, 0),
	(247, 'turism', 'Навчальний інститут туризму та рекреалогії', 89, 0),
	(251, 'medfarm', 'Кафедра фармацевтичних дисциплін', 19, 0),
	(253, 'prikl_physics', 'Кафедра прикладної фізики', 137, 0),
	(254, 'fizyadra', 'Відділення фізики ядра та елементарних частинок', 137, 0),
	(255, 'preuniversity', 'Центр доуніверситетської підготовки та роботи з іноземними громадянами', 130, 0),
	(256, 'ndi_centeurope', 'НДІ Центральної Європи', 89, 0),
	(257, 'forlanguage', 'Кафедра іноземних мов', 132, 0),
	(258, 'zoo_museum', 'Зоологічний музей УжНУ', 89, 0),
	(259, 'logos', 'Центр історично-релігійних студій «Логос»', 89, 0),
	(260, 'stoafter', 'Кафедра стоматології післядипломної освіти', 134, 0),
	(261, 'stokid_after', 'Кафедра стоматології дитячого віку', 134, 0),
	(266, 'history_mus', 'Музей історії УжНУ', 89, 0),
	(267, 'arich_muz', 'Археологічний музей УжНУ', 89, 0),
	(268, 'instbrain', 'НДІ мозку', 89, 0),
	(269, 'ndc_slovistuku', 'Науково-дослідний центр словакістики ', 89, 0),
	(270, 'ndi_phytother', 'НДІ фітотерапії', 89, 0),
	(271, 'ndi_family_med', 'НДІ сімейної медицини', 89, 0),
	(273, 'ndi_derzh_konst', 'НДІ державно-конфесійного права та державно-церковних відносин', 89, 0),
	(306, 'cross_language', 'Кафедра міжмовного спілкування та фахового перекладу', 233, 0),
	(310, 'microbiology', 'Кафедра мікробіології, вірусології та імунології з\\к інфекційних хвороб', 19, 0),
	(317, 'tsc', 'Центр технічного забезпечення', 89, 0);
/*!40000 ALTER TABLE `tbl_department` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_indicator
CREATE TABLE IF NOT EXISTS `tbl_indicator` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UNSIGNED - 0..255 - більше 255 не буде (за замовчуванням - SIGNED -127...127)\\n',
  `position` tinyint(4) unsigned DEFAULT NULL COMMENT 'для сортування у формі (порядковий номер У РОЗДІЛІ (SECTION)',
  `section` tinyint(4) DEFAULT NULL COMMENT 'РОЗДІЛ (звичайний порядковий номер). Їх буде 6-9 розділів - просто зберігати їх назви у масиві додатку - щоб витягувати їх назви на форму.  Створювати окрему таблицю для 6 записів нема сенсу.',
  `title` varchar(255) DEFAULT NULL,
  `hint` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_indicator: ~3 rows (approximately)
DELETE FROM `tbl_indicator`;
/*!40000 ALTER TABLE `tbl_indicator` DISABLE KEYS */;
INSERT INTO `tbl_indicator` (`id`, `position`, `section`, `title`, `hint`) VALUES
	(1, 5, 1, 'Видання підручника для ВНЗ за рекомендацією Вченої ради УжНУ (у календарному році, що завершився)', NULL),
	(2, 1, 1, 'Видання навчального посібника для ВНЗ за рекомендацією Вченої ради УжНУ (у календарному році, що завершився)', NULL),
	(3, 0, 2, 'Видання підручника для загальноосвітньої школи за рекомендацією Вченої ради УжНУ   (у календарному році, що завершився)', NULL);
/*!40000 ALTER TABLE `tbl_indicator` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_person
CREATE TABLE IF NOT EXISTS `tbl_person` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) DEFAULT NULL COMMENT 'Імя',
  `lastname` varchar(50) DEFAULT NULL COMMENT 'Прізвище',
  `secondname` varchar(30) DEFAULT NULL COMMENT 'По-батькові',
  `department_id` smallint(6) DEFAULT NULL COMMENT 'відділ (кафедра), у якому працює людина',
  `user_id` smallint(6) DEFAULT NULL,
  `date_edit` datetime DEFAULT NULL COMMENT 'дата останнього створення\\редагування',
  `status` enum('empty','inprogress','closed') DEFAULT 'empty' COMMENT 'Статус: пусто \\ у процессі заповнення \\ заповнена і закрита для редагування',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_person_tbl_user1_idx` (`user_id`),
  KEY `fk_tbl_person_tbl_department1_idx` (`department_id`),
  CONSTRAINT `fk_tbl_person_tbl_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_person_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_person: ~1 rows (approximately)
DELETE FROM `tbl_person`;
/*!40000 ALTER TABLE `tbl_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_person` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_person_indicator
CREATE TABLE IF NOT EXISTS `tbl_person_indicator` (
  `person_id` smallint(6) NOT NULL,
  `indicator_id` tinyint(4) unsigned NOT NULL,
  `total` float DEFAULT NULL COMMENT 'FLOAT бо будуть не цілі',
  `notes` text COMMENT '"Примітки" з паперової форми - MEMO-поле яке за потребою можна заповнити на формі до кожного з показника для пояснення на основі чого було заповнено цей показник.',
  KEY `fk_tbl_person_indicator_tbl_person1_idx` (`person_id`),
  KEY `fk_tbl_person_indicator_tbl_indicator_idx` (`indicator_id`),
  CONSTRAINT `fk_tbl_person_indicator_tbl_indicator` FOREIGN KEY (`indicator_id`) REFERENCES `tbl_indicator` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_person_indicator_tbl_person1` FOREIGN KEY (`person_id`) REFERENCES `tbl_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_person_indicator: ~0 rows (approximately)
DELETE FROM `tbl_person_indicator`;
/*!40000 ALTER TABLE `tbl_person_indicator` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_person_indicator` ENABLE KEYS */;


-- Dumping structure for table sciencereport.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) DEFAULT NULL COMMENT 'Не повинен бути имейлом - бо у багатьох вже є логіни з сайту університету - це чатсина перед @uzhnu.edu.ua їх офіційної пошти кафедри',
  `password` varchar(255) DEFAULT NULL COMMENT 'тут треба хеш пасворда тримати',
  `email` varchar(50) DEFAULT NULL,
  `preset_dep_id` smallint(6) DEFAULT NULL COMMENT 'Це налаштування для користувача - який відділ\\кафедру він буде заповнювати - але на формі в принципі може вибрати будь який з іншого відділу.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_tbl_user_tbl_department1_idx` (`preset_dep_id`),
  CONSTRAINT `fk_tbl_user_tbl_department1` FOREIGN KEY (`preset_dep_id`) REFERENCES `tbl_department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table sciencereport.tbl_user: ~8 rows (approximately)
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
