<?php
namespace User;

use User\Model\AuthStorage;
use User\Model\DepartmentTable;
use User\Model\IndicatorRelationTable;
use User\Model\IndicatorTable;
use User\Model\PersonTable;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use User\Model\User;
use User\Model\UserTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\Storage;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

include_once 'src/User/Model/User/Model/User.php';
include_once 'src/User/Model/User/Model/Person.php';
include_once 'src/User/Model/User/Model/PersonTable.php';
include_once 'src/User/Model/User/Model/UserTable.php';
include_once 'src/User/Model/User/Model/IndicatorTable.php';
include_once 'src/User/Model/User/Model/DepartmentTable.php';
include_once 'src/User/Model/User/Model/IndicatorRelationTable.php';
include_once 'src/User/Model/User/Model/AuthStorage.php';

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'User\Model\UserTable' =>  function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('tbl_user', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\PersonTable' =>  function($sm) {
                    $tableGateway = $sm->get('PersonTableGateway');
                    $table = new PersonTable($tableGateway);
                    return $table;
                },
                'PersonTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new TableGateway('tbl_person', $dbAdapter);
                },
                'User\Model\AuthStorage' => function ($sm) {
                    return new AuthStorage('user');
                },
                'AuthService' => function ($sm) {
                    $dbAdapter          = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter(
                        $dbAdapter,
                        'tbl_user', 'login', 'password', 'MD5(?)'
                    );
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('User\Model\AuthStorage'));
                    return $authService;
                },
                'User\Model\IndicatorTable' =>  function($sm) {
                    $tableGateway = $sm->get('IndicatorTableGateway');
                    $table = new IndicatorTable($tableGateway);
                    return $table;
                },
                'IndicatorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new TableGateway('tbl_indicator', $dbAdapter);
                },
                'User\Model\DepartmentTable' =>  function($sm) {
                    $tableGateway = $sm->get('DepartmentTableGateway');
                    $table = new DepartmentTable($tableGateway);
                    return $table;
                },
                'DepartmentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new TableGateway(DepartmentTable::TABLE_NAME, $dbAdapter);
                },
                'User\Model\IndicatorRelationTable' =>  function($sm) {
                    $tableGateway = $sm->get('IndicatorRelationTableGateway');
                    $table = new IndicatorRelationTable($tableGateway);
                    return $table;
                },
                'IndicatorRelationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new TableGateway('tbl_person_indicator', $dbAdapter);
                },
            )
        );
    }

}
