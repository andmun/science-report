<?php
namespace User\Controller;

use User\Form\PersonAddForm;
use User\Form\PersonEditForm;
use User\Form\UserLoginForm;
use User\Form\UserRegisterForm;
use User\Model\Person;
use User\Model\User;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class UserController extends AbstractController
{
    protected $_userLoginForm;
    protected $_userRegisterForm;
    protected $_storage;

    public function loginAction()
    {
        if ($this->_getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('user');
        }

        $form = $this->getLoginForm();

        return array(
            'form'      => $form,
            'messages'  => $this->flashmessenger()->getMessages()
        );
    }

    public function registerAction()
    {
        if ($this->_getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('user');
        }
        $form = $this->getUserRegisterForm();

        return array(
            'form'      => $form,
            'departments' => $this->getDepartmentTable()->fetchAsTree(),
            'messages'  => $this->flashmessenger()->getMessages()
        );
    }

    public function registrationAction()
    {
        /** @var \Zend\Form\Form $form */
        $form = $this->getUserRegisterForm();
        $redirect = 'login';

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                if (md5($request->getPost('password')) !== md5($request->getPost('confirm_password'))) {
                    $this->flashmessenger()->addMessage('Повторний пароль не співпадає з введеним.');
                    return $this->redirect()->toRoute('user', array('action' => 'register'));
                }

                try {
                    $user->setEmail($request->getPost('email'))
                        ->setLogin($request->getPost('login'))
                        ->setPassword($request->getPost('password'))
                        ->setPresetDepId($request->getPost('preset_dep_id'));
                    $this->getUserTable()->addUser($user);
                    $this->flashmessenger()->addMessage('Реєстрація пройдена успішно.');
                } catch (\Exception $e) {
                    $this->flashmessenger()->addMessage($e->getMessage());
                    return $this->redirect()->toRoute('user', array('action' => 'register'));
                }
            } else {
                foreach ($form->getMessages() as $messages) {
                    foreach ($messages as $message) {
                        $this->flashmessenger()->addMessage($message);
                    }
                }
                return $this->redirect()->toRoute('user', array('action' => 'register'));
            }
        }
        return $this->redirect()->toRoute($redirect);
    }

    public function authenticateAction()
    {
        $form     = $this->getLoginForm();
        $redirect = 'user';

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()){
            $form->setData($request->getPost());
            if ($form->isValid()){
                $this->_getAuthService()->getAdapter()
                    ->setIdentity($request->getPost('login'))
                    ->setCredential($request->getPost('password'));

                $result = $this->_getAuthService()->authenticate();
                foreach($result->getMessages() as $message) {
                    $this->flashmessenger()->addMessage($message);
                }

                if ($result->isValid()) {
                    $redirect = 'user';
                    if ($request->getPost('rememberMe') == 1 ) {
                        $this->_getSessionStorage()
                            ->setRememberMe(1);
                        $this->_getAuthService()->setStorage($this->_getSessionStorage());
                    }
                    $this->_getAuthService()->getStorage()->write($request->getPost('login'));
                }
            }
        }

        return $this->redirect()->toRoute($redirect);
    }

    public function logoutAction()
    {
        $this->_getSessionStorage()->forgetMe();
        $this->_getAuthService()->clearIdentity();

        $this->flashmessenger()->addMessage('Ви вийшли з профілю.');
        return $this->redirect()->toRoute('login');
    }


    public function indexAction()
    {
        $login = $this->_getAuthService()->getStorage()->read();
        $userId = $this->getUserTable()->load($login, 'login')->getId();

        return new ViewModel(array(
            'persons' => $this->getPersonTable()->fetchByUserId($userId),
        ));
    }

    public function addAction()
    {
        $form = new PersonAddForm();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $person = new Person();
            $form->setInputFilter($person->getInputFilter());
            $form->setData($post);

            if ($form->isValid()) {
                $person->exchangeArray($form->getData());
                $person->setDepartmentId($post['department_id']);
                $this->getPersonTable()->savePerson($person, $this->_getUserId());
                return $this->redirect()->toRoute('user');
            }
        }
        /** @var User $user */
        $user = $this->getCurrentUser();
        return array(
            'form' => $form,
            'departments' => $this->getDepartmentTable()->fetchAsTree(),
            'department_id' => $user->getPresetDepId(),
        );
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('user', array(
                'action' => 'add'
            ));
        }
        try {
            /** @var Person $person */
            $person = $this->getPersonTable()->load($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('user', array(
                'action' => 'index'
            ));
        }

        $form  = new PersonEditForm();

        $form->bind($person);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $personObject = new Person();
            $form->setInputFilter($personObject->getInputFilter());
            $form->setData($post = $request->getPost());

            if ($form->isValid()) {
                $personObject->exchangeArray($person);
                $personObject->setDepartmentId($post['department_id']);
                $this->getPersonTable()->savePerson($personObject, $this->_getUserId());
                return $this->redirect()->toRoute('user');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'departments' => $this->getDepartmentTable()->fetchAsTree(),
            'department_id' => $person['department_id'],
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$this->_isRelatedPerson($id)) {
            return $this->redirect()->toRoute('user');
        }
        if (!$id) {
            return $this->redirect()->toRoute('user');
        }
        $redirect = 'user';
        $this->getPersonTable()->deletePerson($id);
        $this->flashmessenger()->addMessage('Викладач успішно видалений.');
        return $this->redirect()->toRoute($redirect);
    }

    public function onDispatch(MvcEvent $event)
    {
        if (!$this->_getAuthService()->hasIdentity()
            && $event->getRouteMatch()->getParam('action') !== 'login'
            && $event->getRouteMatch()->getParam('action') !== 'authenticate'
            && $event->getRouteMatch()->getParam('action') !== 'register'
            && $event->getRouteMatch()->getParam('action') !== 'registration'
        ){
            return $this->redirect()->toRoute('user', array('action' => 'login'));
        }
        parent::onDispatch($event);
        return $this;
    }

    /**
     * @return \User\Model\AuthStorage
     */
    protected function _getSessionStorage()
    {
        if (! $this->_storage) {
            $this->_storage = $this->getServiceLocator()
                ->get('User\Model\AuthStorage');
        }

        return $this->_storage;
    }

    public function getLoginForm()
    {
        if (!$this->_userLoginForm) {
            $this->_userLoginForm = new UserLoginForm();
        }

        return $this->_userLoginForm;
    }

    public function getUserRegisterForm()
    {
        if (!$this->_userRegisterForm) {
            $this->_userRegisterForm = new UserRegisterForm();
        }
        return $this->_userRegisterForm;
    }
}
