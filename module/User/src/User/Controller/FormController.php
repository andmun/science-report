<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/15/2015
 * Time: 8:50 PM
 */

namespace User\Controller;

use User\Form\RatingForm;
use User\Model\IndicatorRelationTable;
use User\Model\IndicatorTable;
use User\Model\Person;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class FormController extends AbstractController
{
    protected $_ratingForm;

    /**
     * @var
     */
    protected $_relationTable;

    protected $_sections = array(
        '1' => 'НАУКОВО-МЕТОДИЧНА РОБОТА',
        '2' => 'НАУКОВО-ІННОВАЦІЙНА РОБОТА',
        '3' => 'ПІДГОТОВКА НАУКОВИХ КАДРІВ',
        '4' => 'ОРГАНІЗАЦІЙНО-ВИХОВНА РОБОТА',
        '5' => 'ПРОФЕСІЙНЕ ВИЗНАННЯ',
    );

    public function indexAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        $indicators = array();
        /** @var IndicatorTable $indicatorTable */
        $indicatorTable = $this->getServiceLocator()
            ->get('User/Model/IndicatorTable');
        foreach ($this->_sections as $key => $section) {
            $indicators[$section] = $indicatorTable->fetchBySection($key);
        }

        $form = $this->getRatingForm($indicators);
        $data = array();
        foreach ($this->getRelationTable()->load($id) as $row) {
            $data[$row['indicator_id'] . '[total]'] = $row['total'];
            $data[$row['indicator_id'] . '[notes]'] = $row['notes'];
        }

        $form->setData($data);
        $this->getPersonTable()->updateEditDate($id);

        $row = $this->getPersonTable()->load($id);
        if (Person::STATUS_CLOSED === $row['status']) {
            $form->setStatus(Person::STATUS_CLOSED);
        } else {
            $this->getPersonTable()->setStatus($id, Person::STATUS_INPROGRESS);
        }

        return new ViewModel(
            array(
                'id'         => $id,
                'form'       => $form,
                'indicators' => $indicators,
                'messages'   => $this->flashmessenger()->getMessages(),
            )
        );
    }

    public function submitAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        $indicators = $this->getRequest()->getPost()->toArray();
        unset($indicators['submit']);

        $relationTable = $this->getRelationTable();

        foreach ($indicators as $key => $values) {
            $relationTable->saveRelation(
                $id,
                $key,
                isset($values['total']) ? $values['total'] : 0,
                isset($values['notes']) ? $values['notes'] : ''
            );
        }

        return $this->redirect()->toRoute('user');
    }

    public function closeAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        $this->getPersonTable()->setStatus($id, 'closed');
        return $this->redirect()->toRoute('user');
    }

    public function onDispatch(MvcEvent $event)
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$this->_isRelatedPerson($id)) {
            return $this->redirect()->toRoute('user');
        }
        parent::onDispatch($event);
        return $this;
    }

    /**
     * @return IndicatorRelationTable
     */
    public function getRelationTable()
    {
        if (null === $this->_relationTable) {
            $this->_relationTable = $this->getServiceLocator()
                ->get('User/Model/IndicatorRelationTable');
        }
        return $this->_relationTable;
    }

    public function getRatingForm($indicators)
    {
        if (null === $this->_ratingForm) {
            $this->_ratingForm = new RatingForm($indicators);
        }

        return $this->_ratingForm;
    }
}
