<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/15/2015
 * Time: 9:30 PM
 */

namespace User\Controller;


use User\Model\DepartmentTable;
use User\Model\PersonTable;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;

class AbstractController extends AbstractActionController
{
    protected $_authService;
    protected $_personTable;
    protected $_userTable;
    protected $_departmentTable;

    /**
     * @return AuthenticationService
     */
    protected function _getAuthService()
    {
        if (!$this->_authService) {
            $this->_authService = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->_authService;
    }

    protected function _isRelatedPerson($personId)
    {
        $userId = $this->_getUserId();
        if (in_array($personId, $this->getPersonTable()->getRelatedPersons($userId))) {
            return true;
        }
        return false;
    }

    /**
     * @return PersonTable
     */
    public function getPersonTable()
    {
        if (!$this->_personTable) {
            $sm = $this->getServiceLocator();
            $this->_personTable = $sm->get('User\Model\PersonTable');
        }
        return $this->_personTable;
    }

    protected function _getUserId()
    {
        return $this->getUserTable()->getIdByLogin(
            $this->_getAuthService()->getStorage()->read()
        );
    }

    protected function getCurrentUser()
    {
        $userTable = $this->getUserTable();
        return $userTable->load($this->_getAuthService()->getStorage()->read(), 'login');
    }

    /**
     * @return \User\Model\UserTable
     */
    public function getUserTable()
    {
        if (!$this->_userTable) {
            $sm = $this->getServiceLocator();
            $this->_userTable = $sm->get('User\Model\UserTable');
        }
        return $this->_userTable;
    }

    /**
     * @return DepartmentTable
     */
    public function getDepartmentTable()
    {
        if (!$this->_departmentTable) {
            $sm = $this->getServiceLocator();
            $this->_departmentTable = $sm->get('User\Model\DepartmentTable');
        }
        return $this->_departmentTable;
    }
}
