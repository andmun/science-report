<?php

namespace User\View\Helper;
use User\Model\AuthStorage;
use Zend\Authentication\AuthenticationService;
use Zend\Form\View\Helper\AbstractHelper;

/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/18/2015
 * Time: 1:34 PM
 */
class UserIdentity extends AbstractHelper
{
    protected $_serviceManager;

    public function __invoke()
    {
        $authService = new AuthenticationService();
        $authService->setStorage(new AuthStorage('user'));
        return $authService->hasIdentity();
    }

    /**
     * @return mixed
     */
    public function getServiceManager()
    {
        return $this->_serviceManager;
    }

    /**
     * @param mixed $serviceManager
     * @return $this
     */
    public function setServiceManager($serviceManager)
    {
        $this->_serviceManager = $serviceManager;
        return $this;
    }
}
