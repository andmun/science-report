<?php

namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class IndicatorTable
{
    const TABLE_NAME = 'tbl_indicator';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * @param int $section
     */
    public function fetchBySection($section)
    {
        $select = new Select(self::TABLE_NAME);
        $select->where(array('section' => $section))->order('position');
        return $this->tableGateway->selectWith($select);
    }

    public function getIndicator($id)
    {
        $id  = (int) $id;
        $rowSet = $this->tableGateway->select(array('id' => $id));
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

//    public function getCodeIdArray()
//    {
//        $pairs = array();
//        $fetched =  $this->tableGateway->select(function (Select $select) {
//            $select->columns(array('id', 'code'));
//        });
//        foreach ($fetched as $element) {
//            $pairs[$element->code] = $element->id;
//        }
//        return $pairs;
//    }
}
