<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/14/2015
 * Time: 4:57 PM
 */

namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class PersonTable
{
    const TABLE_NAME = 'tbl_person';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function load($id)
    {
        $id  = (int) $id;
        $rowSet = $this->tableGateway->select(array('id' => $id));
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchByUserId($userId)
    {
        $userId = (int) $userId;
        $select = new Select(array('u' => self::TABLE_NAME));
        $select->where(array('u.user_id' => $userId))
            ->join(
                array('d' => DepartmentTable::TABLE_NAME),
                'u.department_id = d.id',
                array('title')
            );
        $rowSet = $this->tableGateway->selectWith($select);
        return $rowSet;
    }

    public function updateEditDate($personId)
    {
        $editDate = date('Y-m-d h:m:s');
        $this->tableGateway->update(
            array('date_edit' => $editDate),
            array('id' => $personId)
        );
    }

    public function savePerson(Person $person, $userId)
    {
        $data = array(
            'firstname'  => $person->getFirstName(),
            'secondname' => $person->getSecondName(),
            'lastname'   => $person->getLastName(),
            'user_id'    => $userId,
            'department_id' => $person->getDepartmentId(),
            'date_edit' => date('Y-m-d h:m:s'),
        );

        $id = (int) $person->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->load($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function deletePerson($personId)
    {
        $this->tableGateway->delete(array('id' => (int) $personId));
    }

    public function getRelatedPersons($userId)
    {
        $select = new Select(self::TABLE_NAME);
        $select->columns(array('id'))->where(array('user_id' => $userId));
        $resultSet = $this->tableGateway->selectWith($select)->toArray();
        $result = array();
        foreach ($resultSet as $column) {
            $result[] = (int) $column['id'];
        }
        return $result;
    }

    public function setStatus($id, $status)
    {
        $this->tableGateway->update(array('status' => $status), array('id' => (int) $id));
    }
}
