<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/16/2015
 * Time: 8:12 PM
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class DepartmentTable
{
    const TABLE_NAME = 'tbl_department';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchAsTree()
    {
        return $this->getTree($this->fetchAll()->toArray());
    }

    public function getTree(array $departments)
    {
        $children = array();
        foreach ($departments as &$item) {
            $children[$item['parent_id']][] = &$item;
        }
        unset($item);

        foreach ($departments as &$item) {
            if (isset($children[$item['id']])) {
                $item['children'] = $children[$item['id']];
            }
        }
        return $children[0];
    }
}
