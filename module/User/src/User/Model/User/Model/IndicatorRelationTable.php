<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/15/2015
 * Time: 11:12 PM
 */

namespace User\Model;

use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\TableGateway;

class IndicatorRelationTable
{
    const TABLE_NAME = 'tbl_person_indicator';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveRelation($personId, $indicatorId, $total, $note)
    {
        if ($this->checkPair($personId, $indicatorId)) {
            $update = new Update(self::TABLE_NAME);
            $update->where(array('person_id' => $personId, 'indicator_id' => $indicatorId))
                ->set(array('total' => $total, 'notes' => $note));
            $this->tableGateway->updateWith($update);
        } else {
            $insert = new Insert(self::TABLE_NAME);
            $insert->values(
                array(
                    'person_id'    => $personId,
                    'indicator_id' => $indicatorId,
                    'total'        => $total,
                    'notes'        => $note
                )
            );
            $this->tableGateway->insertWith($insert);
        }
        return $this;
    }

    public function checkPair($personId, $indicatorId)
    {
        $select = new Select(self::TABLE_NAME);
        $select->where(array('person_id' => $personId, 'indicator_id' => $indicatorId));
        $rowSet = $this->tableGateway->selectWith($select);
        return $rowSet->count() >= 1 ? true : false;
    }

    public function load($personId)
    {
        return $this->tableGateway->select(array(
            'person_id' => $personId,
        ));
    }
}
