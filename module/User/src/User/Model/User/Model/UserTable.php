<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 5/21/2015
 * Time: 5:31 PM
 */

namespace User\Model;
use Zend\Db\TableGateway\TableGateway;

class UserTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function load($id, $field = null)
    {
        $rowSet = $this->tableGateway->select(array($field => $id));
        if (1 < $rowSet->count()) {
            return $rowSet;
        }
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id for $field");
        }
        return $row;
    }

    public function saveUser(User $user)
    {
        $data = array(
            'email' => $user->getEmail(),
            'login'  => $user->getLogin(),
            'password'  => md5($user->getPassword()),
            'preset_dep_id' => $user->getPresetDepId(),
        );

        $id = (int) $user->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->load($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function addUser(User $user)
    {
        $data = array(
            'email' => $user->getEmail(),
            'login'  => $user->getLogin(),
            'password'  => md5($user->getPassword()),
            'preset_dep_id' => $user->getPresetDepId(),
        );
        try {
            $this->tableGateway->insert($data);
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'Duplicate entry')) {
                throw new \Exception('Such user already exists.');
            }
            throw new \Exception('Error while adding new user.');
        }
        return $this;
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

    public function getIdByEmail($email)
    {
        $rowset = $this->tableGateway->select(array('email' => $email));
        /** @var User $row */
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with email '$email'");
        }
        return $row->getId();
    }

    public function getIdByLogin($login)
    {
        $rowset = $this->tableGateway->select(array('login' => $login));
        /** @var User $row */
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row with login '$login'");
        }
        return $row->getId();
    }
}
