<?php

namespace User\Form;

use Zend\Form\Form;

class UserLoginForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'login',
            'type' => 'Text',
            'options' => array(
                'label' => 'Логін'
            ),
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Введіть ваш логін',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Text',
            'options' => array(
                'label' => 'Пароль',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'type' => 'password',
                'placeholder' => 'Введіть ваш пароль',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Ввійти',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}
