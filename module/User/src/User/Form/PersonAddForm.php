<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/15/2015
 * Time: 12:14 AM
 */

namespace User\Form;

use Zend\Form\Form;

class PersonAddForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'firstname',
            'type' => 'Text',
            'options' => array(
                'label' => 'Ім\'я'
            ),
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Введіть ім\'я',
            ),
        ));

        $this->add(array(
            'name' => 'secondname',
            'type' => 'Text',
            'options' => array(
                'label' => 'Прізвище'
            ),
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Введіть прізвище',
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'type' => 'Text',
            'options' => array(
                'label' => 'По-батькові'
            ),
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Введіть по-батькові',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Додати',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}
