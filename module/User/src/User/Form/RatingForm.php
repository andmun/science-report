<?php
/**
 * Created by PhpStorm.
 * User: a.muntian
 * Date: 6/15/2015
 * Time: 9:36 PM
 */

namespace User\Form;

use Zend\Form\Form;

class RatingForm extends Form
{
    protected $_status;

    public function setStatus($status)
    {
        $this->_status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function __construct($sections, $name = null)
    {
        parent::__construct($name);

        foreach ($sections as $name => $section) {
            $this->add(
              array(
                  'name' => 'section_' . $name,
                  'type' => 'Text',
                  'options' => array(
                      'label' => $name
                  ),
                  'attributes' => array(
                      'hidden' =>  true,
                  ),
              )
            );
            foreach ($section as $key => $indicator) {
                $this->add(
                    array(
                        'name'       => $indicator['id'] . '[total]',
                        'type'       => 'Number',
                        'options'    => array(
                            'label' => $indicator['title'],
                            'hint'  => $indicator['hint']
                        ),
                        'attributes' => array(
                            'type'        => 'number',
                            'class'       => 'form-control',
                            'placeholder' => 'Введіть кількість балів',
                        ),
                    )
                );
                $this->add(
                    array(
                        'name'       => $indicator['id'] . '[notes]',
                        'type'       => 'Textarea',
                        'options'    => array(
                            'label' => 'Нотатки',
                        ),
                        'attributes' => array(
                            'type'        => 'textarea',
                            'class'       => 'form-control',
                            'placeholder' => 'Введіть нотатки',
                        ),
                    )
                );
            }
        }

        $this->add(array(
            'name'       => 'submit',
            'type'       => 'Submit',
            'attributes' => array(
                'value' => 'Зберегти форму',
                'id'    => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}
