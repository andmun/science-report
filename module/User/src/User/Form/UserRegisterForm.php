<?php
namespace User\Form;

use Zend\Form\Form;

class UserRegisterForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('admin');

        $this->add(array(
            'name' => 'login',
            'type' => 'Text',
            'options' => array(
                'label' => 'Логін'
            ),
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Введіть ваш логін',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Пароль',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'type' => 'password',
                'placeholder' => 'Введіть ваш пароль',
            ),
        ));

        $this->add(array(
            'name' => 'confirm_password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Введіть пароль ще раз',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'type' => 'password',
                'placeholder' => 'Введіть ваш пароль',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Email',
            'options' => array(
                'label' => 'Електронна адреса'
            ),
            'attributes' => array(
                'class' => 'form-control required',
                'type' => 'email',
                'placeholder' => 'Введіть вашу електронну адресу'
            ),
        ));

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'department',
//            'options' => array(
//                'label' => 'Кафедра',
//                'empty_option' => 'Виберіть кафедру',
//                'value_options' => array(
//                    '0' => 'Math',
//                    '1' => 'Engineer',
//                    '2' => 'Physics',
//                    '3' => 'Something',
//                ),
//            ),
//            'attributes' => array(
//                'class' => 'form-control',
//                'type' => 'select',
//            ),
//        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Зареєструватись',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));
    }
}
